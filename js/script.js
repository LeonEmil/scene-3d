//'use strict'

import * as THREE from "/build/three.module.js"
import { OrbitControls } from "/jsm/controls/OrbitControls.js"

// Variables
let container, sceneWidth, sceneHeight, scene, renderer, camera, controls, cube

// Functions

const init = () => {
    createScene()
    update()
}

const createScene = () => {
    sceneWidth = window.innerWidth
    sceneHeight = window.innerHeight

    scene = new THREE.Scene()
    scene.background = new THREE.Color( 0x000000)

    renderer = new THREE.WebGLRenderer({antialias: true, alpha: true})
    renderer.setPixelRatio(window.devicePixelRatio)
    renderer.setSize( sceneWidth, sceneHeight)

    container = document.getElementById("container")
    container.appendChild(renderer.domElement)

    camera = new THREE.PerspectiveCamera(60, sceneWidth / sceneHeight, 1, 1000)
    camera.position.set(0, 0, 50)

    let light = new THREE.DirectionalLight(0xffffff)
    light.position.set(0, 0, 1)
    scene.add(light)

    let hemi = new THREE.HemisphereLight(0xffffff, 0xffffff, 0.5)
    hemi.position.set(0, 0, 5)
    scene.add(hemi)

    controls = new OrbitControls(camera, renderer.domElement)
    controls.update()
 
    let cubeGeo = new THREE.BoxGeometry(10, 10, 10)
    let cubeMaterial = new THREE.MeshPhongMaterial({color: 0x003399})
    cube = new THREE.Mesh(cubeGeo, cubeMaterial)
    scene.add(cube)
}

const update = () => {
    requestAnimationFrame(update)
    render()
}

const render = () => {
    controls.update()
    renderer.render(scene, camera)
    cube.rotation.y +=  0.01
    cube.rotation.x += 0.01
}

addEventListener("resize", () => {
    sceneWidth = window.innerWidth
    sceneHeight = window.innerHeight
    renderer.setPixelRatio(window.devicePixelRatio)
    renderer.setSize(sceneWidth, sceneHeight)
    //renderer.render(scene, camera)
})

init()